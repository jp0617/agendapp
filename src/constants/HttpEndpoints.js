export const USERS = {
    login: 'auth/signin',
    getUsers: 'users',
    check:'auth/check'
    
}

export const TASKS = {
    getTasks: 'tasks',
    createTask: 'tasks/create'
}
